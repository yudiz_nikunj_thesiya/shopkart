import React from "react";
import CartProduct from "../assets/components/CartProduct";
import "../assets/styles/cart.scss";
import { getCartTotal } from "../state/reducer";
import { useStateValue } from "../state/StateProvider";

const Cart = () => {
	const [{ cart }, dispatch] = useStateValue();

	return (
		<div className="cart">
			<div className="cart__header">
				<span className="cart__header-heading">
					You have {cart?.length} Items in Your Cart.
				</span>
			</div>
			<div className="cart__items">
				{cart?.map((item) => (
					<CartProduct
						key={item.id}
						id={item.id}
						category={item.category}
						title={item.title}
						img={item.img}
						price={item.price}
						rating={item.ratings}
						quantity={item.quantity}
						itemTotalAmt={item.itemTotalAmt}
					/>
				))}
			</div>
		</div>
	);
};

export default Cart;
