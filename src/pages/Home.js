import React, { useEffect, useState } from "react";

import Product from "../assets/components/Product";
import { BsArrowDown } from "react-icons/bs";
import { FiSearch } from "react-icons/fi";
import "../assets/styles/home.scss";
import "../assets/styles/herosection.scss";

const Home = () => {
	const [allProducts, setAllProducts] = useState([]);
	const [searchTerm, setSearchTerm] = useState("");
	const [search, setSearch] = useState("");

	useEffect(() => {
		fakeApiData();
	});

	const fakeApiData = async () => {
		const response = await fetch("https://fakestoreapi.com/products");
		const jsonData = await response.json();
		setAllProducts(jsonData);
	};

	return (
		<div className="home">
			<div className="banner">
				<span className="banner__title">Find the best products you want.</span>
				<form
					className="banner__search"
					onSubmit={(e) => {
						e.preventDefault();
						setSearch(searchTerm);
					}}
				>
					<input
						type="text"
						className="banner__search--input"
						placeholder="Search Items"
						value={searchTerm}
						onChange={(e) => setSearchTerm(e.target.value)}
					/>
					<button
						type="submit"
						className="banner__search--btn"
						onClick={(e) => {
							e.preventDefault();
							setSearch(searchTerm);
						}}
					>
						<FiSearch />
					</button>
				</form>
				<div className="banner__scroll">
					<span className="banner__scroll-title">SCROLL DOWN</span>
					<a href="#allProducts">
						<BsArrowDown className="banner__scroll-icon" />
					</a>
				</div>
			</div>
			<h1 className="heading">All Products</h1>

			<div className="products" id="allProducts">
				{allProducts
					.filter((item) => {
						if (item.title.toLowerCase().includes(search)) {
							return item;
						}
					})
					.map((item) => (
						<Product
							id={item.id}
							key={item.id}
							title={item.title}
							img={item.image}
							category={item.category}
							price={item.price}
							ratings={item.rating.rate}
							quantity={1}
							itemTotalAmt={item.price}
						/>
					))}
			</div>
		</div>
	);
};

export default Home;
