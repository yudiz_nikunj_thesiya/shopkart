import React from "react";
import { RiShoppingCartFill } from "react-icons/ri";
import { IoIosPerson } from "react-icons/io";
import { Link } from "react-router-dom";
import "../styles/nav.scss";
import { useStateValue } from "../../state/StateProvider";

const Nav = () => {
	// const [state, dispatch] = useStateValue();
	const [{ cart }, dispatch] = useStateValue();

	return (
		<div className="nav">
			<Link to="/" className="nav__logo">
				ShopKart
			</Link>

			<div className="nav__menu">
				<Link to="/cart" className="nav__menu--cart">
					<RiShoppingCartFill className="nav__menu--cart-icon" />
					<span className="nav__menu--cart-label">{cart?.length}</span>
				</Link>
				<div className="nav__menu--account">
					<IoIosPerson className="nav__menu--account-icon" />
				</div>
			</div>
		</div>
	);
};

export default Nav;
