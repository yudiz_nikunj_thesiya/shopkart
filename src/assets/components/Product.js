import React, { useState } from "react";
import { MdOutlineStarPurple500 } from "react-icons/md";
import PropTypes from "prop-types";
import "../styles/product.scss";
import { useStateValue } from "../../state/StateProvider";
import { Link } from "react-router-dom";

const Product = ({
	id,
	title,
	img,
	price,
	ratings,
	category,
	quantity,
	itemTotalAmt,
}) => {
	const [btnClick, setBtnClick] = useState(false);
	const [{ cart }, dispatch] = useStateValue();
	const addToCart = () => {
		dispatch({
			type: "ADD_TO_CART",
			item: {
				id: id,
				title: title,
				category: category,
				price: price,
				ratings: ratings,
				img: img,
				quantity: quantity,
				itemTotalAmt: itemTotalAmt * quantity,
			},
		});
		setBtnClick(true);
	};
	return (
		<div className="product">
			<span className="product__category">{category}</span>
			<div className="product__img-container">
				<img src={img} alt={title} className="" />
			</div>
			<span className="product__title">{title}</span>
			{ratings > 4 ? (
				<div className="product__rating">
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
				</div>
			) : ratings > 3 ? (
				<div className="product__rating">
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
				</div>
			) : ratings > 2 ? (
				<div className="product__rating">
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
				</div>
			) : ratings > 1 ? (
				<div className="product__rating">
					<MdOutlineStarPurple500 />
					<MdOutlineStarPurple500 />
				</div>
			) : (
				<div className="product__rating">
					<MdOutlineStarPurple500 />
				</div>
			)}
			<div className="product__price">
				<span>₹</span>
				<span>{price}</span>
			</div>
			{btnClick ? (
				<Link to="cart" className="product__btn">
					Go to Cart
				</Link>
			) : (
				<button onClick={() => addToCart()} className="product__btn">
					Add to Cart
				</button>
			)}
		</div>
	);
};

Product.propTypes = {
	id: PropTypes.number,
	title: PropTypes.string,
	img: PropTypes.string,
	category: PropTypes.string,
	price: PropTypes.number,
	ratings: PropTypes.number,
	quantity: PropTypes.number,
	itemTotalAmt: PropTypes.number,
};

export default Product;
