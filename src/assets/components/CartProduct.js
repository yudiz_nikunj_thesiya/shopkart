import React, { useState } from "react";
import PropTypes from "prop-types";
import { MdOutlineStarPurple500, MdDelete } from "react-icons/md";
import "../styles/cartproduct.scss";
import { useStateValue } from "../../state/StateProvider";

export const totalOrderAmount = [];

const CartProduct = ({
	id,
	title,
	img,
	rating,
	price,
	category,
	quantity,
	itemTotalAmt,
}) => {
	const [{ cart }, dispatch] = useStateValue();
	const [cartQuantity, setCartQuantity] = useState(quantity);

	const incrementItem = () => {
		setCartQuantity((prevCount) => prevCount + 1);
	};
	const decrementItem = () => {
		setCartQuantity((prevCount) => prevCount - 1);
	};

	const removeFromCart = () => {
		dispatch({
			type: "REMOVE_FROM_CART",
			id: id,
		});
	};
	return (
		<div className="cartproduct">
			<div className="cartproduct__img-container">
				<img src={img} alt={title} className="" />
			</div>
			<div className="cartproduct__detail-container">
				<span className="cartproduct__detail-container-category">
					{category}
				</span>
				<span className="cartproduct__detail-container-title">{title}</span>
				{rating > 4 ? (
					<div className="cartproduct__detail-container-rating">
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
					</div>
				) : rating > 3 ? (
					<div className="cartproduct__detail-container-rating">
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
					</div>
				) : rating > 2 ? (
					<div className="cartproduct__detail-container-rating">
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
					</div>
				) : rating > 1 ? (
					<div className="cartproduct__detail-container-rating">
						<MdOutlineStarPurple500 />
						<MdOutlineStarPurple500 />
					</div>
				) : (
					<div className="cartproduct__detail-container-rating">
						<MdOutlineStarPurple500 />
					</div>
				)}
				<div className="cartproduct__detail-container-price">
					<span>₹</span>
					<span>
						{price} x {cartQuantity} = <span>₹</span>
						{itemTotalAmt * cartQuantity}
					</span>
				</div>
				<div className="cartproduct__detail-container-counter">
					{cartQuantity === 1 ? (
						<button onClick={() => removeFromCart()}>
							<MdDelete />
						</button>
					) : (
						<button onClick={() => decrementItem()}> - </button>
					)}

					<span>{cartQuantity}</span>
					<button onClick={() => incrementItem()}> + </button>
				</div>
				<button
					className="cartproduct__detail-container-btn"
					onClick={() => removeFromCart()}
				>
					Remove Item
				</button>
			</div>
		</div>
	);
};

CartProduct.propTypes = {
	id: PropTypes.number,
	title: PropTypes.string,
	img: PropTypes.string,
	category: PropTypes.string,
	price: PropTypes.number,
	rating: PropTypes.number,
	quantity: PropTypes.number,
	itemTotalAmt: PropTypes.number,
};

export default CartProduct;
